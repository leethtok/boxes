const express = require('express');
const router = express.Router();
const reqlib = require('app-root-path').require;
const logger = reqlib('logger');

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var recentDays = 5;

var schema = {
  "type": "array",
  "minItems": 10,
  "maxItems": 20,
  "items": {
	  "type": "object",
	  "properties": {
	  	"image":{
	  	  "type": "image",
	      "faker": "random.image"
	  	}, 
	    "name": {
	      "type": "string",
	      "faker": "lorem.word"
	    },
	    "description": {
	      "type": "string",
	      "faker": "lorem.paragraph"
	    },
	    "rating" : {
	      "type": "integer", 
	       "minimum": 1,
  		   "maximum": 5
	    },
	},
	  "required": [
	    "image",
	    "name", 
	    "description",
	    "rating"
	   ]
	  }
};

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   logger.debug(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('boxes', 
	  	{  boxes:  sample });
  });

  
});

module.exports = router;
