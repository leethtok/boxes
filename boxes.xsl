<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>BOXES</h1>
                <p>Total:<xsl:value-of select="count(//box)"/>,
                Price: <xsl:value-of select="sum(//box/@price)"/>
                </p>

            </body>
        </html>
    </xsl:template>
    <xsl:template match="box">
        <div>
        <img scr="{@img}"/>
        <span>Name :<xsl:value-of select="@name"/></span>
        <span>Price :<xsl:value-of select="@price"/></span>
        <span>Category :<xsl:value-of select="@category"/></span>
        <span>Description :<xsl:value-of select="@description"/></span>
        </div>
    </xsl:template>
</xsl:stylesheet>