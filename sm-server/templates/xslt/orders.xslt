<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:key name="boxes-by-category" match="box" use="@category" />

    <xsl:template match="/">
        <div>
            <h1>BOXES</h1>
                <!-- //student[count(. |  key('students-by-group', @group)[1] ) = 1 -->
                <!-- //student[@group=9] -->
                <h4>Total boxs:<xsl:value-of select="count(//box)"/></h4>

                <xsl:apply-templates select="//box[generate-id(.) = generate-id(key('boxes-by-category', @category)[1] )]"/>
        </div>
    </xsl:template>

    <xsl:template match="box">
        <h5>Boxes <xsl:value-of select="@category"/></h5>
        <h6>Total: <xsl:value-of select="count(//box[@category = current()/@category])"/></h6>
        <ul>
            <xsl:apply-templates select="key('boxes-by-category', @category)" mode="grouping"/>
        </ul>
    </xsl:template>

    <xsl:template match="box" mode="grouping">
        <li><xsl:value-of select="@price"/> -
            <xsl:value-of select="@name"/>
        </li>
    </xsl:template>

</xsl:stylesheet>